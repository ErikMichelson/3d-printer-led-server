module 3dprinter-leds

go 1.17

require (
	github.com/fasthttp/router v1.4.3
	github.com/joho/godotenv v1.4.0
	github.com/rpi-ws281x/rpi-ws281x-go v1.0.8
	github.com/valyala/fasthttp v1.30.0
)

require (
	github.com/andybalholm/brotli v1.0.3 // indirect
	github.com/klauspost/compress v1.13.6 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/savsgio/gotils v0.0.0-20210921075833-21a6215cb0e4 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
)

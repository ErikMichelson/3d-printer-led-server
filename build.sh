#!/bin/sh
dir=$( cd -- "$(dirname "$0")" >/dev/null 2>&1 || exit ; pwd -P )
docker run --rm -v "$dir:/usr/src/zled" --platform linux/arm/v7 -w "/usr/src/zled" registry.liltv.media/ws2811-builder:latest go build -ldflags "-s -w" -o "build/printer-leds" -v cmd/3dprinter-leds.go

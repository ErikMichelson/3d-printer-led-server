package main

import (
  "os"

  "3dprinter-leds/internal/config"
  "3dprinter-leds/internal/routing"
  "3dprinter-leds/internal/runtime"
  "3dprinter-leds/internal/ws281x"
  _ "github.com/joho/godotenv/autoload"
  "github.com/valyala/fasthttp"
)

func main() {
  config.Initialize()
  ws281x.Setup()
  defer ws281x.Shutdown()
  routing.SetupRouter()
  compressMiddleware := fasthttp.CompressHandler(routing.HandleRequest)
  server := fasthttp.Server{
    CloseOnShutdown: true,
    MaxConnsPerIP:   16,
    Handler:         compressMiddleware,
  }
  println("3D-Printer LED server starting on port " + runtime.Port)
  err := server.ListenAndServe(":" + runtime.Port)
  if err != nil {
    println(err.Error())
    os.Exit(1)
  }
  os.Exit(0)
}

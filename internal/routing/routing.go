package routing

import (
  "3dprinter-leds/internal/handlers"
  "github.com/fasthttp/router"
  "github.com/valyala/fasthttp"
)

var appRouter = router.New()

func SetupRouter () {
  appRouter.GET("/utils/color", handlers.HandleGetWs281xColor)
  appRouter.PUT("/lights/printer", handlers.HandleSetPrintLightState)
  appRouter.PUT("/lights/status", handlers.HandleSetStatusLightState)
  appRouter.PUT("/print/status", handlers.HandleSetPrintState)
  appRouter.GET("/debug/state", handlers.HandleGetDebugState)
}

func HandleRequest (ctx *fasthttp.RequestCtx) {
  ctx.Response.Header.SetContentType("text/plain")
  appRouter.Handler(ctx)
}
package handlers

import (
  "strconv"

  "3dprinter-leds/internal/structs"
  "3dprinter-leds/internal/utils"
  "github.com/valyala/fasthttp"
)

func HandleGetWs281xColor(ctx *fasthttp.RequestCtx) {
  rRaw := string(ctx.QueryArgs().Peek("r"))
  gRaw := string(ctx.QueryArgs().Peek("g"))
  bRaw := string(ctx.QueryArgs().Peek("b"))
  r, errR := strconv.Atoi(rRaw)
  g, errG := strconv.Atoi(gRaw)
  b, errB := strconv.Atoi(bRaw)
  if errR != nil || errG != nil || errB != nil {
    ctx.Response.Header.SetStatusCode(400)
    ctx.Response.SetBodyString("Error: Query parameters r, g and b need to be numbers from 0 to 255.")
    return
  }
  converted := utils.ColorToWs281x(structs.Color{
    R: uint8(r),
    G: uint8(g),
    B: uint8(b),
  })
  convertedStr := strconv.Itoa(int(converted))
  ctx.Response.SetBodyString(convertedStr)
  return
}

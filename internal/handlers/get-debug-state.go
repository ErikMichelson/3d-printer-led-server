package handlers

import (
  "3dprinter-leds/internal/runtime"
  "3dprinter-leds/internal/structs"
  "3dprinter-leds/internal/utils"
  "github.com/valyala/fasthttp"
)

func HandleGetDebugState(ctx *fasthttp.RequestCtx) {
  utils.SendJsonResponse(ctx, structs.DebugState{
    StatusLedMode:   runtime.StatusLedMode,
    StatusLedColor:  runtime.StatusLedColor,
    PrinterLedMode:  runtime.PrinterLedMode,
    PrinterLedColor: runtime.PrinterLedColor,
    PrintState:      runtime.PrintState,
    PrintProgress:   runtime.PrintProgress,
  })
}

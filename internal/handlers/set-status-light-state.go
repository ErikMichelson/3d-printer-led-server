package handlers

import (
  "encoding/json"

  "3dprinter-leds/internal/constants"
  "3dprinter-leds/internal/runtime"
  "3dprinter-leds/internal/structs"
  "3dprinter-leds/internal/utils"
  "3dprinter-leds/internal/ws281x"
  "github.com/valyala/fasthttp"
)

func HandleSetStatusLightState(ctx *fasthttp.RequestCtx) {
  var data structs.RequestLightState
  err := json.Unmarshal(ctx.PostBody(), &data)
  if err != nil {
    ctx.Response.SetStatusCode(400)
    ctx.SetBodyString("Error: Invalid data provided. Required: json containing 'mode' and optional 'color'.")
    return
  }
  switch data.Mode {
  case "off":
    runtime.StatusLedMode = constants.LightModeStatusOff
    break
  case "progress":
    runtime.StatusLedMode = constants.LightModeStatusProgress
    break
  case "warn":
    runtime.StatusLedMode = constants.LightModeStatusWarn
  case "idle":
    runtime.StatusLedMode = constants.LightModeStatusIdle
    break
  case "error":
    runtime.StatusLedMode = constants.LightModeStatusError
    break
  case "color":
    color := utils.ColorToWs281x(data.Color)
    runtime.StatusLedColor = color
    runtime.StatusLedMode = constants.LightModeStatusColor
    break
  default:
    ctx.Response.SetStatusCode(400)
    ctx.SetBodyString("Error: Invalid 'mode' provided. Valid values are: 'off', 'idle', 'warn', 'error', 'progress' or 'color'.")
    return
  }
  ws281x.UpdateWs281xLeds()
  ctx.Response.SetStatusCode(204)
}

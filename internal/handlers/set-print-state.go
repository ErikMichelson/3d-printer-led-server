package handlers

import (
  "encoding/json"

  "3dprinter-leds/internal/constants"
  "3dprinter-leds/internal/runtime"
  "3dprinter-leds/internal/structs"
  "3dprinter-leds/internal/ws281x"
  "github.com/valyala/fasthttp"
)

func HandleSetPrintState(ctx *fasthttp.RequestCtx) {
  var data structs.RequestPrintState
  err := json.Unmarshal(ctx.PostBody(), &data)
  if err != nil {
    ctx.Response.SetStatusCode(400)
    ctx.SetBodyString("Error: Invalid data provided. Required: json containing 'percent', 'state' and 'topic'.")
    return
  }
  runtime.PrintProgress = int(data.Percent)
  if data.State.Text == "Offline" {
    runtime.PrintState = constants.PrintStateUnknown
  }
  if data.State.Flags.Printing {
    runtime.PrintState = constants.PrintStateRunning
  }
  if data.State.Flags.Paused {
    runtime.PrintState = constants.PrintStatePaused
  }
  if data.State.Flags.Error {
    runtime.PrintState = constants.PrintStateFailed
  }
  ws281x.UpdateWs281xLeds()
  ctx.Response.SetStatusCode(204)
}

package handlers

import (
  "encoding/json"

  "3dprinter-leds/internal/constants"
  "3dprinter-leds/internal/runtime"
  "3dprinter-leds/internal/structs"
  "3dprinter-leds/internal/utils"
  "3dprinter-leds/internal/ws281x"
  "github.com/valyala/fasthttp"
)

func HandleSetPrintLightState(ctx *fasthttp.RequestCtx) {
  var data structs.RequestLightState
  err := json.Unmarshal(ctx.PostBody(), &data)
  if err != nil {
    ctx.Response.SetStatusCode(400)
    ctx.SetBodyString("Error: Invalid data provided. Required: json containing 'mode' and optional 'color'.")
    return
  }
  switch data.Mode {
  case "off":
    runtime.PrinterLedMode = constants.LightModePrinterOff
    break
  case "white":
    runtime.PrinterLedMode = constants.LightModePrinterWhite
    break
  case "idle":
    runtime.PrinterLedMode = constants.LightModePrinterColor
    runtime.PrinterLedColor = constants.ColorIdleWhite
    break
  case "color":
    color := utils.ColorToWs281x(data.Color)
    runtime.PrinterLedColor = color
    runtime.PrinterLedMode = constants.LightModePrinterColor
    break
  default:
    ctx.Response.SetStatusCode(400)
    ctx.SetBodyString("Error: Invalid 'mode' provided. Valid values are: 'off', 'white', 'idle' or 'color'.")
    return
  }
  ws281x.UpdateWs281xLeds()
  ctx.Response.SetStatusCode(204)
}

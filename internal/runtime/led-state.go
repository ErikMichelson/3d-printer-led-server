package runtime

import "3dprinter-leds/internal/constants"

var StatusLedMode = constants.LightModeStatusProgress
var StatusLedColor = constants.ColorOff
var PrinterLedMode = constants.LightModePrinterColor
var PrinterLedColor = constants.ColorIdleWhite

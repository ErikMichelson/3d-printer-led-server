package structs

type RequestLightState struct {
  Mode string `json:"mode"`
  Color Color `json:"color"`
}

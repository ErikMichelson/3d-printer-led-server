package structs

type OctoprintStateFlags struct {
  Operational   bool `json:"operational"`
  Printing      bool `json:"printing"`
  Cancelling    bool `json:"cancelling"`
  Pausing       bool `json:"pausing"`
  Resuming      bool `json:"resuming"`
  Finishing     bool `json:"finishing"`
  ClosedOrError bool `json:"closedOrError"`
  Error         bool `json:"error"`
  Paused        bool `json:"paused"`
  Ready         bool `json:"ready"`
  SdReady       bool `json:"sdReady"`
}

type OctoprintState struct {
  Text  string              `json:"text"`
  Flags OctoprintStateFlags `json:"flags"`
}

type RequestPrintState struct {
  Topic   string         `json:"topic"`
  Percent float32        `json:"percent"`
  State   OctoprintState `json:"state"`
}

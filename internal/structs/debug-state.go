package structs

type DebugState struct {
  StatusLedMode   int    `json:"status_led_mode"`
  StatusLedColor  uint32 `json:"status_led_color"`
  PrinterLedMode  int    `json:"printer_led_mode"`
  PrinterLedColor uint32 `json:"printer_led_color"`
  PrintState      int    `json:"print_state"`
  PrintProgress   int    `json:"print_progress"`
}

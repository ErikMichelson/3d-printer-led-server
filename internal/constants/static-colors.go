package constants

const (
  ColorOff         uint32 = 0
  ColorFullWhite   uint32 = 15790320
  ColorIdleWhite   uint32 = 3947580
  ColorStatusIdle  uint32 = 52479
  ColorStatusWarn  uint32 = 16754176
  ColorStatusError uint32 = 16711680
)

package constants

const (
  PrintStateUnknown = iota
  PrintStateRunning
  PrintStatePaused
  PrintStateFailed
)

package constants

const (
  LightModeStatusOff = iota
  LightModeStatusProgress
  LightModeStatusIdle
  LightModeStatusWarn
  LightModeStatusError
  LightModeStatusColor
)

const (
  LightModePrinterOff = iota
  LightModePrinterWhite
  LightModePrinterColor
)

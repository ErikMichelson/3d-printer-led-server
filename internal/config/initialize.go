package config

import (
  "os"
  "strconv"

  "3dprinter-leds/internal/runtime"
)

func Initialize () {
  port, set := os.LookupEnv("PORT")
  if set {
    runtime.Port = port
  }
  ledCountRaw, set := os.LookupEnv("LED_COUNT")
  if set {
    ledCount, err := strconv.Atoi(ledCountRaw)
    if err != nil {
      println("Error: Env variable LED_COUNT is invalid.")
      os.Exit(1)
    }
    runtime.LedCount = ledCount
  }
  ledCountPrinterRaw, set := os.LookupEnv("LED_COUNT_PRINTER")
  if set {
    ledCountPrinter, err := strconv.Atoi(ledCountPrinterRaw)
    if err != nil {
      println("Error: Env variable LED_COUNT_PRINTER is invalid.")
      os.Exit(1)
    }
    runtime.LedCountPrinter = ledCountPrinter
  }
  ledFreqRaw, set := os.LookupEnv("LED_FREQ")
  if set {
    ledFreq, err := strconv.Atoi(ledFreqRaw)
    if err != nil {
      println("Error: Env variable LED_FREQ is invalid.")
      os.Exit(1)
    }
    runtime.LedFrequency = ledFreq
  }
  gpioPinRaw, set := os.LookupEnv("LED_PIN")
  if set {
    gpioPin, err := strconv.Atoi(gpioPinRaw)
    if err != nil {
      println("Error: Env variable LED_PIN is invalid.")
      os.Exit(1)
    }
    runtime.GpioPin = gpioPin
  }
}

package ws281x

import (
  "os"
  "time"

  "3dprinter-leds/internal/constants"
  "3dprinter-leds/internal/runtime"
  libWs281x "github.com/rpi-ws281x/rpi-ws281x-go"
)

var wsInstance *libWs281x.WS2811

func Setup () {
  var err error
  options := libWs281x.DefaultOptions
  options.Channels[0].Brightness = 255
  options.Channels[0].GpioPin = runtime.GpioPin
  options.Channels[0].LedCount = runtime.LedCount
  options.Frequency = runtime.LedFrequency
  wsInstance, err = libWs281x.MakeWS2811(&options)
  if err != nil {
    println("Can not create GPIO channel for ws281x hardware. Do you have the right permissions?")
    os.Exit(1)
  }
  err = wsInstance.Init()
  if err != nil {
    println("Can not initialize ws281x hardware.")
    os.Exit(1)
  }
  runInitAnimation()
}

func runInitAnimation () {
  colors := []uint32{constants.ColorFullWhite, constants.ColorStatusError, constants.ColorStatusWarn, constants.ColorStatusIdle, constants.ColorOff}
  for colorIndex := 0; colorIndex < len(colors); colorIndex++ {
    for i := 0; i < runtime.LedCount; i++ {
      wsInstance.Leds(0)[i] = colors[colorIndex]
      err := wsInstance.Render()
      if err != nil {
        println("Error rendering init animation")
        os.Exit(1)
      }
      time.Sleep(50 * time.Millisecond)
    }
    time.Sleep(200 * time.Millisecond)
  }
}

func Shutdown () {
  wsInstance.Fini()
}
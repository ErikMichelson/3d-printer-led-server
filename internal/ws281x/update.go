package ws281x

import (
  "math"

  "3dprinter-leds/internal/constants"
  "3dprinter-leds/internal/runtime"
)

func UpdateWs281xLeds () {
  switch runtime.PrinterLedMode {
  case constants.LightModePrinterOff:
    setPrinterLedsAll(constants.ColorOff)
    break
  case constants.LightModePrinterWhite:
    setPrinterLedsAll(constants.ColorFullWhite)
    break
  case constants.LightModePrinterColor:
    setPrinterLedsAll(runtime.PrinterLedColor)
    break
  }
  switch runtime.StatusLedMode {
  case constants.LightModeStatusOff:
    setStatusLedsAll(constants.ColorOff)
    break
  case constants.LightModeStatusIdle:
    setStatusLedsAll(constants.ColorStatusIdle)
    break
  case constants.LightModeStatusError:
    setStatusLedsAll(constants.ColorStatusError)
    break
  case constants.LightModeStatusColor:
    setStatusLedsAll(runtime.StatusLedColor)
    break
  case constants.LightModeStatusProgress:
    updateProgressStatusLeds()
    break
  }
  err := wsInstance.Render()
  if err != nil {
    println("Error rendering LED stripe: ", err.Error())
  }
}

func setPrinterLedsAll (color uint32) {
  println("Setting all printer leds to color: ", color)
  for i := 0; i < runtime.LedCountPrinter; i++ {
    wsInstance.Leds(0)[i] = color
  }
}

func setStatusLedsAll (color uint32) {
  println("Setting all status leds to color: ", color)
  for i := runtime.LedCountPrinter; i < runtime.LedCount; i++ {
    wsInstance.Leds(0)[i] = color
  }
}

func setStatusLedsPercent (color uint32, progress int, progressBar bool) {
  factorActive := float64(progress) / 100.0
  preciseActive := factorActive * float64(runtime.LedCount - runtime.LedCountPrinter)
  ledsFullActivePrecise, ledRemainderPercentPrecise := math.Modf(preciseActive)
  ledsFullActive := int(ledsFullActivePrecise)
  ledRemainderPercent := int(math.Round(ledRemainderPercentPrecise * 10))
  println("Updating status LEDs: ", ledsFullActive, "(active)", ledRemainderPercent, "(remainder)")

  for i := runtime.LedCountPrinter; i < ledsFullActive + runtime.LedCountPrinter; i++ {
    wsInstance.Leds(0)[i] = color
  }
  for i := runtime.LedCountPrinter + ledsFullActive; i < runtime.LedCount; i++ {
    wsInstance.Leds(0)[i] = constants.ColorOff
  }
  if ledsFullActive < (runtime.LedCount + 1) && progressBar && ledRemainderPercent > 0 {
    wsInstance.Leds(0)[runtime.LedCountPrinter + ledsFullActive] = constants.ColorProgress[ledRemainderPercent - 1]
  }
}

func setStatusLedsProgressPercent (progress int) {
  setStatusLedsPercent(constants.ColorProgress[9], progress, true)
}

func updateProgressStatusLeds () {
  switch runtime.PrintState {
  case constants.PrintStateUnknown:
    setStatusLedsAll(constants.ColorStatusIdle)
    break
  case constants.PrintStatePaused:
    setStatusLedsPercent(constants.ColorStatusWarn, runtime.PrintProgress, false)
    break
  case constants.PrintStateFailed:
    setStatusLedsPercent(constants.ColorStatusError, runtime.PrintProgress, false)
    break
  case constants.PrintStateRunning:
    setStatusLedsProgressPercent(runtime.PrintProgress)
    break
  default:
    setStatusLedsAll(constants.ColorOff)
    break
  }
}

package utils

import (
  "encoding/json"

  "github.com/valyala/fasthttp"
)

func SendJsonResponse (ctx *fasthttp.RequestCtx, obj interface{}) {
  ctx.Response.SetStatusCode(200)
  ctx.Response.Header.SetContentType("application/json")
  if err := json.NewEncoder(ctx).Encode(obj); err != nil {
    ctx.Error(err.Error(), fasthttp.StatusInternalServerError)
  }
}

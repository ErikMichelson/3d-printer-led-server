package utils

import "3dprinter-leds/internal/structs"

func ColorToWs281x (color structs.Color) uint32 {
  return uint32(color.R)<<16 | uint32(color.G)<<8 | uint32(color.B)
}
